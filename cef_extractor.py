"""Course-end Feedback CSV Splitter

This script splits a CSV file containing course-end feedback results into
separate files based on course number. It is assumed that the first row of the
input file is the column labels.
"""

import csv
import os.path
import sys
from itertools import groupby
from operator  import itemgetter
from tkinter import Tk
from tkinter import ttk
from tkinter.filedialog import askopenfilename
from pandas import DataFrame

NORM_FONT = ("Verdana", 10)

def popupmsg(msg):
    """Show a popup message."""

    popup = Tk()
    popup.wm_title("!")
    popup.eval('tk::PlaceWindow . center')
    label = ttk.Label(popup, text=msg, font=NORM_FONT)
    label.pack(side="top", fill="x", pady=10)
    B1 = ttk.Button(popup, text="Ok", command = popup.destroy)
    B1.pack()
    popup.mainloop()

def get_file_dlg():
    """Return the input file path using GUI."""

    Tk().withdraw()
    return askopenfilename()

def group_by_course(file_path):
    """Read data file and return data rows grouped by course.

    This yields a groupby iterator containing (course, data) pairs, where the
    data part is a grouper iterator. Each item in the grouper object is a
    dictionary with keys from the survey columns.
    """

    with open(file_path) as f:
        return groupby(sorted(csv.DictReader(f), key=itemgetter('crs_number')),
                        key=itemgetter('crs_number'))

def build_survey(all_responses):
    """Build new table of survey responses.

    The survey data provided as input has one line for each question response,
    so a single survey spans multiple lines where each question is in a column
    labeled 'question' and the response is in a column labeled 'response'.

    This function consolidates all the lines associated with a single survey id
    into one dictionary and associates each response with a key in the
    dictionary matching the question string. This facilitates putting all the
    responses from a single survey on one line in the final csv file.

    The survey dictionaries for the entire course are consolidated into the
    returned list.
    """

    result = []
    for sid, responses in groupby(sorted(all_responses, key=itemgetter('part_svyid')),
                                key=itemgetter('part_svyid')):
        new_dict = {}
        for answer in responses:
            # determine new dict keys from response and question field
            for k, v in answer.items():
                if k == 'question':
                    new_key = v
                elif k == 'response':
                    stored_value = v
                elif k != 'response_descr' and k not in new_dict:
                    new_dict[k] = v
            new_dict[new_key] = stored_value
        result.append(new_dict)

    return result

def write_df(path, filename, course_num, course_data):
    """Write course data file from pandas DataFrame."""

    outfile_path = os.path.join(path, '_'.join([course_num, filename]))
    course_data.to_csv(outfile_path, index=False)

def main():
    """Main function."""

    popupmsg("Select the input file.")
    infile_path = get_file_dlg()
    all_courses_data = group_by_course(infile_path)
    for course, course_data in all_courses_data:
        course_survey = build_survey(course_data)
        course_df = DataFrame(course_survey)
        write_df(*os.path.split(infile_path), course, course_df)

if (__name__ == '__main__'):
    main()