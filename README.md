# CEF Processor

## Description
Splits a large CSV file containing course-end feedback survey responses into course-specific files and translates the responses so that a single survey spanning multiple rows becomes one row per response.